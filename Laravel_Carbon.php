    public function chartData(Request $request)
    {
        Carbon::setWeekStartsAt(Carbon::SUNDAY);
        Carbon::setWeekEndsAt(Carbon::SATURDAY);
        $type = $request->type;
        $now = Carbon::now();
        $year = $now->year;
        $date = Carbon::today()->subDays(7)->toDateString();
//        $dateE = Carbon::now()->startOfMonth();
        $clicks = '';
        $impressions = '';
        $cpc = '';
        $costs = '';
        $months = '';
        $total_clicks = 0;
        $total_impressions = 0;
        $avg_cpc = 0;
        $total_cost = 0;
        if ($type =='t'){
            $visitor = AdvReports::where('adv_id',Auth::id())
                ->whereDate('created_at',Carbon::today())
                ->select(
                    DB::raw("date(created_at) as date"),
                    DB::raw("SUM(adv_ad_total_click) as total_click"),
                    DB::raw("SUM(adv_ad_total_spent) as total_cost"),
                    DB::raw("SUM(adv_ad_total_impression) as total_impression"))
                ->orderBy("created_at","asc")
                ->groupBy(DB::raw("date"))
                ->get();
        }
        elseif ($type =='tm'){
            $visitor =AdvReports::where('adv_id',Auth::id())
                ->whereYear('created_at', $year)
                ->whereMonth('created_at', Carbon::now()->month)
                ->select(
                    DB::raw("date(created_at) as date"),
                    DB::raw("SUM(adv_ad_total_click) as total_click"),
                    DB::raw("SUM(adv_ad_total_spent) as total_cost"),
                    DB::raw("SUM(adv_ad_total_impression) as total_impression"))
                ->orderBy("created_at","asc")
                ->groupBy(DB::raw("date"))
                ->get();
        }
        elseif ($type =='lm'){
            $first_day_last_month = new DateTime('first day of last month');
            $month = $first_day_last_month->format('m');
            $visitor = AdvReports::where('adv_id',Auth::id())
                ->whereYear('created_at', $year)
                ->whereMonth('created_at', $month)
                ->select(
                    DB::raw("date(created_at) as date"),
                    DB::raw("SUM(adv_ad_total_click) as total_click"),
                    DB::raw("SUM(adv_ad_total_spent) as total_cost"),
                    DB::raw("SUM(adv_ad_total_impression) as total_impression"))
                ->orderBy("created_at","asc")
                ->groupBy(DB::raw("date"))
                ->get();
        }
        elseif ($type =='y'){
            $visitor = AdvReports::where('adv_id',Auth::id())
                ->whereDate('created_at',Carbon::yesterday())
                ->select(
                    DB::raw("date(created_at) as date"),
                    DB::raw("SUM(adv_ad_total_click) as total_click"),
                    DB::raw("SUM(adv_ad_total_spent) as total_cost"),
                    DB::raw("SUM(adv_ad_total_impression) as total_impression"))
                ->orderBy("created_at","asc")
                ->groupBy(DB::raw("date"))
                ->get();
        }
        elseif ($type =='tw'){
            $visitor = AdvReports::where('adv_id',Auth::id())
                ->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
                ->select(
                    DB::raw("date(created_at) as date"),
                    DB::raw("SUM(adv_ad_total_click) as total_click"),
                    DB::raw("SUM(adv_ad_total_spent) as total_cost"),
                    DB::raw("SUM(adv_ad_total_impression) as total_impression"))
                ->orderBy("created_at","asc")
                ->groupBy(DB::raw("date"))
                ->get();
        }
        elseif ($type =='lw'){
            $visitor = AdvReports::where('adv_id',Auth::id())
                ->whereBetween('created_at', [Carbon::now()->subWeek(1)->startOfWeek(),Carbon::now()->subWeek(1)->endOfWeek()])
                ->select(
                    DB::raw("date(created_at) as date"),
                    DB::raw("SUM(adv_ad_total_click) as total_click"),
                    DB::raw("SUM(adv_ad_total_spent) as total_cost"),
                    DB::raw("SUM(adv_ad_total_impression) as total_impression"))
                ->orderBy("created_at","asc")
                ->groupBy(DB::raw("date"))
                ->get();
        }
        else{
            $visitor = AdvReports::where('adv_id',Auth::id())
                ->where('created_at', '>=', Date($date))
                ->select(
                    DB::raw("date(created_at) as date"),
                    DB::raw("SUM(adv_ad_total_click) as total_click"),
                    DB::raw("SUM(adv_ad_total_spent) as total_cost"),
                    DB::raw("SUM(adv_ad_total_impression) as total_impression"))
                ->orderBy("created_at","asc")
                ->groupBy(DB::raw("date"))
                ->get();
        }
        foreach($visitor as $row)
        {
            $click = $row->total_click;
            $impression = $row->total_impression;
            $cost = $row->total_cost;
            $cost1 = number_format($cost,2);
            if ($click!=0){
                $cpc_p = $cost/$click;
                $cpc1 = number_format($cpc_p,2);
            }
            else{
                $cpc_p = 0;
                $cpc1 = 0;
            }
            $month = date('M d',strtotime($row->date));
            $clicks = $clicks.$click.',';
            $impressions = $impressions.$impression.',';
            $cpc = $cpc.$cpc1.',';
            $costs = $costs.$cost1.',';
            $months = $months.'"'.$month.'",';
            $total_clicks +=$click;
            $total_impressions +=$impression;
            $avg_cpc +=$cpc_p;
            $total_cost +=$cost;
        }
        $clicks = trim($clicks,",");
        $impressions = trim($impressions,",");
        $cpc = trim($cpc,",");
        $costs = trim($costs,",");
        $months = trim($months,",");
        $data =  view('advertiser.chartDashboard')
            ->with('total_clicks',$total_clicks)
            ->with('total_impressions',$total_impressions)
            ->with('avg_cpc',$avg_cpc)
            ->with('total_cost',$total_cost)
            ->with('click',$clicks)
            ->with('impression',$impressions)
            ->with('cpc',$cpc)
            ->with('cost',$costs)
            ->with('month',$months)->render();
        return response()->json(['success' =>$data]);
    }