[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Drawing") 
[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") 

$runFor = 10
# Here 10 is 10 Minutes
$cursorChangeInterval = 20
# Here 70 is 70 Seconds

Clear-Host
$StartTime=(GET-DATE)
Write-Host 'PS D:\xampp\htdocs\laravel6> ' -NoNewline;
Write-Host 'n' -ForegroundColor Yellow -NoNewline;
Start-Sleep -Milliseconds 350
Write-Host 'p' -ForegroundColor Yellow -NoNewline;
Start-Sleep -Milliseconds 300
Write-Host 'm' -ForegroundColor Yellow -NoNewline;
Start-Sleep -Milliseconds 250
Write-Host ' ' -ForegroundColor Yellow -NoNewline;
Start-Sleep -Milliseconds 200
Write-Host 'r' -NoNewline;
Start-Sleep -Milliseconds 150
Write-Host 'u' -NoNewline;
Start-Sleep -Milliseconds 300
Write-Host 'n' -NoNewline;
Start-Sleep -Milliseconds 150
Write-Host ' ' -NoNewline;
Start-Sleep -Milliseconds 300
Write-Host 'w' -NoNewline;
Start-Sleep -Milliseconds 150
Write-Host 'a' -NoNewline;
Start-Sleep -Milliseconds 150
Write-Host 't' -NoNewline;
Start-Sleep -Milliseconds 150
Write-Host 'c' -NoNewline;
Start-Sleep -Milliseconds 250
Write-Host 'h' -NoNewline;
Start-Sleep -Milliseconds 500
Write-Host '

> @ watch D:\xampp\htdocs\laravel6
> npm run development -- --watch


> @ development D:\xampp\htdocs\mediusware\tispedisco
> cross-env NODE_ENV=development node_modules/webpack/bin/webpack.js --progress --hide-modules --config=node_modules/laravel-mix/setup/webpack.config.js "--watch"
'

$WShell = New-Object -com "Wscript.Shell"

while ($true) {
    $p1 = [System.Windows.Forms.Cursor]::Position
    Start-Sleep -Seconds $cursorChangeInterval  # or use a shorter intervall with the -milliseconds parameter
    $p2 = [System.Windows.Forms.Cursor]::Position
    if($p1.X -eq $p2.X -and $p1.Y -eq $p2.Y) {
        $EndTime=(GET-DATE)
        $diff = [System.Math]::Floor(($EndTime - $StartTime).TotalSeconds)
        if($diff -le ($runFor*60)){
            $WShell.sendkeys("%+{ESC}")

            Clear-Host
            $time = Get-Date -uformat "%I:%M:%S"
            Write-Host 'PS D:\xampp\htdocs\laravel6> ' -NoNewline;
            Write-Host 'npm ' -ForegroundColor Yellow -NoNewline;
            Write-Host 'run watch'
            Write-Host '
> @ watch D:\xampp\htdocs\laravel6
> npm run development -- --watch


> @ development D:\xampp\htdocs\mediusware\tispedisco
> cross-env NODE_ENV=development node_modules/webpack/bin/webpack.js --progress --hide-modules --config=node_modules/laravel-mix/setup/webpack.config.js "--watch"
'
            Start-Sleep -Seconds 1
            Write-Host '10% building 1/1 modules 0 active' -ForegroundColor Yellow;
            Write-Host 'webpack is watching the files�'
            Start-Sleep -Seconds 2
            Write-Host ''
            $perc = Get-Random -Minimum 87 -Maximum 98
            $randMS = Get-Random -Minimum 2500 -Maximum 3000
            Write-Host $perc -NoNewline;
            Write-Host '% after emitting SizeLimitsPlugin' -ForegroundColor Yellow;
            Write-Host ''
            Start-Sleep -Seconds 2
            Write-Host ' DONE ' -ForegroundColor Black -BackgroundColor DarkGreen -NoNewline;
            Write-Host " Compiled successfully in $randMS" -ForegroundColor DarkGreen -NoNewline;
            Write-Host "ms                                                                        " -ForegroundColor DarkGreen -NoNewline;
            Write-Host "$time PM" -ForegroundColor DarkGray
            Write-Host '       Asset      Size   Chunks             Chunk Names'
            Write-Host '/css/app.css  ' -ForegroundColor DarkGreen -NoNewline;
            Write-Host '62 bytes  /js/app  ' -NoNewline;
            Write-Host '[emitted]' -ForegroundColor DarkGreen -NoNewline;
            Write-Host '  /js/app'
            Write-Host '  /js/app.js  ' -ForegroundColor DarkGreen -NoNewline;
            Write-Host '1.38 MiB  /js/app  ' -NoNewline;
            Write-Host '[emitted]' -ForegroundColor DarkGreen -NoNewline;
            Write-Host '  /js/app'
        }

    } else {
        $StartTime=(GET-DATE)
        $EndTime=(GET-DATE)
    }
    #if($p1.X -eq $p2.X -and $p1.Y -eq $p2.Y){
    #    $WShell.sendkeys("%+{ESC}")
    #    Start-Sleep -Seconds 5
    #}else {
    #    echo "The mouse did not move"
    #    Start-Sleep -Seconds 5
    #}
}