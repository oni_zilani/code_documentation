-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 29, 2019 at 07:37 PM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `saptagao_data_sap`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `donors` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `email` varchar(120) NOT NULL,
  `password` varchar(120) NOT NULL,
  `usergroup` varchar(250) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `mobile` varchar(64) DEFAULT NULL,
  `blood_id` int(11) DEFAULT NULL,
  `image` text DEFAULT NULL,
  `last_login` int(11) DEFAULT NULL,
  `last_ip` varchar(19) DEFAULT NULL,
  `last_browser` varchar(255) DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT 1,
  `deleted` tinyint(1) DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `usergroup`, `address`, `mobile`, `image`, `last_login`, `last_ip`, `last_browser`, `enabled`, `deleted`, `created_at`, `updated_at`) VALUES
(2, 'Oni', 'oni.cse21@gmail.com', '$2y$12$4hgEj3n7dCPq.EEm7Ns8NOkiTwo3KBC3p4nKJ3Y7bN3qbP02oeUaq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2019-09-11 13:56:17', '2019-09-11 13:56:17');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
