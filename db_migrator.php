<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$old_db_name = 'sparktask_stagic';
$team_db_name = 'compltit_team_';//compltit_team_
$servername = '127.0.0.1';
$username = 'root';
$password = '';

try {
    $conn = new PDO("mysql:host=$servername;dbname=$old_db_name", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


    $sql_teams = "SELECT * FROM teams;";
    $result_team = $conn->query($sql_teams);
    if ($result_team->rowCount() > 0) {
        while ($team = $result_team->fetch(PDO::FETCH_OBJ)) {
            try {
                $dbh = new PDO("mysql:host=$servername", $username, $password);
                $team_db = $team_db_name . $team->id;

                //create database
                $dbh->exec("CREATE DATABASE `$team_db`;
               CREATE USER '$username'@'localhost' IDENTIFIED BY '$password';
               GRANT ALL ON `$team_db`.* TO '$username'@'localhost';
               FLUSH PRIVILEGES;");
                //create tables
                CreateTeamDB($team_db, $servername, $username, $password);

                $team_db_connect = new PDO("mysql:host=$servername;dbname=$team_db", $username, $password);

                $sql_user = "SELECT * FROM users where id = " . $team->owner_id;
                $result_user = $conn->query($sql_user);
                if ($result_user->rowCount() > 0) {
                    $user = $result_user->fetch(PDO::FETCH_OBJ);
                    $sql = "INSERT INTO `users_details` (`user_id`,`name`,`email`,`email_verified_at`,`photo_url`,`country_code`,`phone`,`last_read_announcements_at`,`created_at`,`updated_at`) 
                                VALUES ('$team->owner_id','$user->name','$user->email','$user->email_verified_at','$user->photo_url','$user->country_code','$user->phone','$user->last_read_announcements_at','$user->created_at','$user->updated_at')";
                    $team_db_connect->query($sql);

                }

                $sql_tags = "SELECT * FROM tags where team_id = " . $team->id;
                $result_tags = $conn->query($sql_tags);
                if ($result_tags->rowCount() > 0) {
                    while ($tagData = $result_tags->fetch(PDO::FETCH_OBJ)) {
                        $sql = "INSERT INTO tags (`id`, `team_id`, `title`, `color`, `created_at`, `updated_at`)
                               VALUES ('$tagData->id', '$tagData->team_id', '$tagData->title', '$tagData->color', '$tagData->created_at', '$tagData->updated_at')";
                        $team_db_connect->query($sql);
                    }
                }

                $sql_projects = "SELECT * FROM projects where team_id = " . $team->id;
                $result_project = $conn->query($sql_projects);

                if ($result_project->rowCount() > 0) {
                    while ($project = $result_project->fetch(PDO::FETCH_OBJ)) {
                        $sql = "INSERT INTO projects (id, team_id, name, description, created_by, updated_by)
                               VALUES ('$project->id', $project->team_id, '$project->name', '$project->description', $project->created_by, '$project->updated_by')";
                        $team_db_connect->query($sql);


                        $sql_project_users = "SELECT * FROM project_users where project_id = " . $project->id;
                        $result_project_users = $conn->query($sql_project_users);
                        if ($result_project_users->rowCount() > 0) {
                            while ($users_data = $result_project_users->fetch(PDO::FETCH_OBJ)) {
                                $sql = "INSERT INTO project_users (`id`, `project_id`, `user_id`, `status`, `created_at`, `updated_at`)
                                   VALUES ('$users_data->id', '$users_data->project_id', '$users_data->user_id', '$users_data->status', '$users_data->created_at', '$users_data->updated_at')";
                                $team_db_connect->query($sql);

                                $sql_user = "SELECT * FROM users where id = " . $users_data->user_id;
                                $result_user = $conn->query($sql_user);
                                if ($result_user->rowCount() > 0) {
                                    $user = $result_user->fetch(PDO::FETCH_OBJ);
                                    $sql = "INSERT INTO `users_details` (`user_id`,`name`,`email`,`email_verified_at`,`photo_url`,`country_code`,`phone`,`last_read_announcements_at`,`created_at`,`updated_at`) 
                                            VALUES ('$user->id','$user->name','$user->email','$user->email_verified_at','$user->photo_url','$user->country_code','$user->phone','$user->last_read_announcements_at','$user->created_at','$user->updated_at')";
                                    $team_db_connect->query($sql);

                                }
                            }
                        }

                        $sql_project_rules = "SELECT * FROM rules where project_id = " . $project->id;
                        $result_project_rules = $conn->query($sql_project_rules);
                        if ($result_project_rules->rowCount() > 0) {
                            while ($rules_data = $result_project_rules->fetch(PDO::FETCH_OBJ)) {
                                $sql = "INSERT INTO rules (`id`, `name`, `status`, `project_id`, `move_from`, `move_to`, `created_by`, `assigned_users`, `created_at`, `updated_at`)
                                   VALUES ('$rules_data->id', '$rules_data->name', '$rules_data->status', '$rules_data->project_id', '$rules_data->move_from', '$rules_data->move_to', '$rules_data->created_by', '$rules_data->assigned_users', '$rules_data->created_at', '$rules_data->updated_at')";
                                $team_db_connect->query($sql);
                            }
                        }

                        $sql_project_navs = "SELECT * FROM project_nav_items where project_id = " . $project->id;
                        $result_nav = $conn->query($sql_project_navs);
                        if ($result_nav->rowCount() > 0) {
                            while ($nav = $result_nav->fetch(PDO::FETCH_OBJ)) {

                                $sql = "INSERT INTO project_nav_items (id, project_id, title, type, sort_id, created_at, updated_at)
                                           VALUES ('$nav->id', '$nav->project_id', '$nav->title', '$nav->type', '$nav->sort_id', '$nav->created_at', '$nav->updated_at')";
                                $team_db_connect->query($sql);
                            }
                        }

                        $sql_project_board = "SELECT * FROM multiple_boards where project_id = " . $project->id;
                        $result_board = $conn->query($sql_project_board);

                        if ($result_board->rowCount() > 0) {
                            while ($board = $result_board->fetch(PDO::FETCH_OBJ)) {
                                $sql = "INSERT INTO multiple_boards (id, project_id, nav_id, board_title, description, created_at, updated_at)
                                           VALUES ('$board->id', '$board->project_id', '$board->nav_id', '$board->board_title', '$board->board_title', '$board->created_at', '$board->updated_at')";
                                $team_db_connect->query($sql);
                            }
                        }

                        $sql_project_Lists = "SELECT * FROM multiple_lists where project_id = " . $project->id;
                        $result_list = $conn->query($sql_project_Lists);

                        if ($result_list->rowCount() > 0) {
                            while ($list = $result_list->fetch(PDO::FETCH_OBJ)) {
                                $sql = "INSERT INTO  multiple_lists (id, project_id, sort_id, nav_id, list_title, description, open, is_delete, created_at, updated_at)
                                           VALUES ('$list->id', '$list->project_id', '$list->sort_id', '$list->nav_id', '$list->list_title', '$list->description', '$list->open', '$list->is_delete', '$list->created_at', '$list->updated_at')";
                                $team_db_connect->query($sql);

                            }
                        }


                        $sql_project_tasks = "SELECT * FROM task_lists where project_id = " . $project->id;
                        $result_tasks = $conn->query($sql_project_tasks);

                        if ($result_tasks->rowCount() > 0) {
                            while ($task = $result_tasks->fetch(PDO::FETCH_OBJ)) {
                                $parent_id = ($task->parent_id !== NULL) ? $task->parent_id : "NULL";
                                $board_parent_id = ($task->board_parent_id !== NULL) ? $task->board_parent_id : "NULL";
                                $list_id = ($task->list_id !== NULL) ? $task->list_id : "NULL";
                                $multiple_board_id = ($task->multiple_board_id !== NULL) ? $task->multiple_board_id : "NULL";
                                $priority_label = ($task->priority_label !== NULL) ? $task->priority_label : "NULL";
                                $task_flag = ($task->task_flag !== NULL) ? $task->task_flag : "NULL";
                                $board_flag = ($task->board_flag !== NULL) ? $task->board_flag : "NULL";
                                $hidden = ($task->hidden !== NULL) ? $task->hidden : "NULL";
                                $color = ($task->color !== NULL) ? $task->color : "NULL";
                                $progress = ($task->progress !== NULL) ? $task->progress : "NULL";
                                $deleted_at = ($task->deleted_at !== NULL) ? $task->deleted_at : "NULL";
                                $copied_from = "NULL";//($task->copied_from !== NULL) ? $task->copied_from : "NULL";

                                $sql = "INSERT INTO task_lists (id, parent_id, project_id, board_parent_id, list_id, multiple_board_id, priority_label, task_flag, board_flag, hidden, sort_id, board_sort_id, created_by, updated_by, open, card_open, title, description, color, progress, date, is_complete, is_deleted, deleted_at, created_at, updated_at, copied_from)
                                           VALUES ($task->id, $parent_id, $task->project_id, $board_parent_id, $list_id, $multiple_board_id, $priority_label, $task_flag, $board_flag, $hidden, $task->sort_id, $task->board_sort_id, $task->created_by, $task->updated_by, $task->open, $task->card_open, '$task->title',
                                           '$task->description', '$color', $progress, '$task->date', $task->is_complete, $task->is_deleted, $deleted_at, '$task->created_at', '$task->updated_at', $copied_from)";

                                $team_db_connect->query($sql);

                                $sql_task_assign = "SELECT * FROM task_assigned_users where task_id = " . $task->id;
                                $result_task_assign = $conn->query($sql_task_assign);
                                if ($result_task_assign->rowCount() > 0) {
                                    while ($task_assigned_users_data = $result_task_assign->fetch(PDO::FETCH_OBJ)) {
                                        $sql = "INSERT INTO task_assigned_users (`id`, `user_id`, `task_id`, `created_by`, `updated_by`, `created_at`, `updated_at`)
                                               VALUES ('$task_assigned_users_data->id', '$task_assigned_users_data->user_id', '$task_assigned_users_data->task_id', '$task_assigned_users_data->created_by', '$task_assigned_users_data->updated_by', '$task_assigned_users_data->created_at', '$task_assigned_users_data->updated_at')";
                                        $team_db_connect->query($sql);
                                    }
                                }
                                //task file
                                $sql_task_file = "SELECT * FROM task_files where tasks_id = " . $task->id;
                                $result_task_file = $conn->query($sql_task_file);
                                if ($result_task_file->rowCount() > 0) {
                                    while ($task_file_data = $result_task_file->fetch(PDO::FETCH_OBJ)) {
                                        $sql_task_check = "SELECT * FROM task_lists where id = " . $task_file_data->tasks_id;
                                        $result_task_check = $team_db_connect->query($sql_task_check);
                                        if ($result_task_check->rowCount() > 0) {
                                            $sql = "INSERT INTO task_files (`id`, `file_name`, `tasks_id`, `created_by`, `updated_by`, `created_at`, `updated_at`)
                                                       VALUES ('$task_file_data->id', '$task_file_data->file_name', '$task_file_data->tasks_id', '$task_file_data->created_by', '$task_file_data->updated_by', '$task_file_data->created_at', '$task_file_data->updated_at')";
                                            $team_db_connect->query($sql);
                                        }
                                    }
                                }
                                //task comments
                                $sql_task_comments = "SELECT * FROM comments where task_id = " . $task->id;
                                $result_task_comments = $conn->query($sql_task_comments);
                                if ($result_task_comments->rowCount() > 0) {
                                    while ($comments_data = $result_task_comments->fetch(PDO::FETCH_OBJ)) {

                                        $sql_task_check = "SELECT * FROM task_lists where id = " . $comments_data->task_id;
                                        $result_task_check = $team_db_connect->query($sql_task_check);
                                        if ($result_task_check->rowCount() > 0) {
                                            $parent_id = ($comments_data->parent_id !== NULL) ? $comments_data->parent_id : "NULL";
                                            $sql = $sql = "INSERT INTO comments (`id`, `task_id`, `user_id`, `parent_id`, `comment`, `attatchment`, `created_at`, `updated_at`)
                                                               VALUES ('$comments_data->id', '$comments_data->task_id', '$comments_data->user_id', $parent_id, '$comments_data->comment','$comments_data->attatchment', '$comments_data->created_at', '$comments_data->updated_at')";
                                            $team_db_connect->query($sql);
                                        }
                                    }
                                }
                                //task tag assign
                                $sql_task_assign_tag = "SELECT * FROM assign_tags where task_id = " . $task->id;
                                $result_task_assign_tag = $conn->query($sql_task_assign_tag);
                                if ($result_task_assign_tag->rowCount() > 0) {
                                    while ($assign_tags_data = $result_task_assign_tag->fetch(PDO::FETCH_OBJ)) {

                                        $sql_task_check = "SELECT * FROM task_lists where id = " . $assign_tags_data->task_id;
                                        $result_task_check = $team_db_connect->query($sql_task_check);
                                        $sql_tags_check = "SELECT * FROM tags where id = " . $assign_tags_data->tag_id;
                                        $result_tags_check = $team_db_connect->query($sql_tags_check);

                                        if ($result_task_check->rowCount() > 0 && $result_tags_check->rowCount() > 0) {
                                            $sql = "INSERT INTO assign_tags (`id`, `task_id`, `tag_id`, `created_at`, `updated_at`)
                                                       VALUES ('$assign_tags_data->id', '$assign_tags_data->task_id', '$assign_tags_data->tag_id', '$assign_tags_data->created_at', '$assign_tags_data->updated_at')";
                                            $team_db_connect->query($sql);
                                        }
                                    }
                                }


                                $sql_link_list = "SELECT * FROM link_list_to_columns where task_list_id = " . $task->id;
                                $result_link_list = $conn->query($sql_link_list);
                                if ($result_link_list->rowCount() > 0) {
                                    while ($link_list_to_columns_data = $result_link_list->fetch(PDO::FETCH_OBJ)) {
                                        $sql = "INSERT INTO `link_list_to_columns` (`id`, `multiple_list_id`, `task_list_id`, `created_at`, `updated_at`) 
                                                    VALUES ('$link_list_to_columns_data->id','$link_list_to_columns_data->multiple_list_id','$link_list_to_columns_data->task_list_id','$link_list_to_columns_data->created_at','$link_list_to_columns_data->updated_at')";
                                        $team_db_connect->query($sql);
                                    }
                                }


                            }
                        }

                        $sql_action_logs = "SELECT * FROM action_logs where project_id = " . $project->id;
                        $result_al_list = $conn->query($sql_action_logs);
                        if ($result_al_list->rowCount() > 0) {
                            while ($list = $result_al_list->fetch(PDO::FETCH_OBJ)) {

                                $multiple_list_id = ($list->multiple_list_id !== NULL) ? $list->multiple_list_id : "NULL";
                                $task_id = ($list->task_id !== NULL) ? $list->task_id : "NULL";
                                $board_id = ($list->board_id !== NULL) ? $list->board_id : "NULL";
                                $multiple_board_id = ($list->multiple_board_id !== NULL) ? $list->multiple_board_id : "NULL";

                                $sql = "INSERT INTO  action_logs (id, project_id, multiple_list_id, task_id, multiple_board_id, board_id, title, log_type, action_type, action_by, action_at)
                                           VALUES ('$list->id', '$list->project_id', $multiple_list_id, $task_id, $multiple_board_id, $board_id, '$list->title', '$list->log_type', '$list->action_type', '$list->action_by', '$list->action_at')";
                                $team_db_connect->query($sql);
                            }
                        }
                    }
                }
            } catch (PDOException $e) {
                echo "Connection failed: " . $e->getMessage();
            }
        }
        echo "Data Copy Successful";
    }

} catch (PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
}


function CreateTeamDB ($db_name, $servername, $username, $password)
{
    $team_db_connect = new PDO("mysql:host=$servername;dbname=$db_name", $username, $password);
    $sql = "CREATE TABLE `action_logs` (
               `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
               `project_id` int(11) DEFAULT NULL,
               `multiple_list_id` bigint(20) DEFAULT NULL,
               `task_id` bigint(20) UNSIGNED DEFAULT NULL,
               `multiple_board_id` bigint(20) DEFAULT NULL,
               `board_id` bigint(20) DEFAULT NULL,
               `title` text COLLATE utf8mb4_unicode_ci,
               `log_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
               `action_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
               `action_by` int(11) NOT NULL,
               `action_at` datetime NOT NULL,
                PRIMARY KEY (`id`)
           ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

            CREATE TABLE `assign_tags` (
             `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
             `task_id` bigint(20) UNSIGNED NOT NULL,
             `tag_id` bigint(20) UNSIGNED NOT NULL,
             `created_at` timestamp NULL DEFAULT NULL,
             `updated_at` timestamp NULL DEFAULT NULL,
             PRIMARY KEY (`id`)
           ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

            CREATE TABLE `comments` (
             `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
             `task_id` bigint(20) UNSIGNED NOT NULL,
             `user_id` bigint(20) NOT NULL,
             `parent_id` bigint(20) DEFAULT NULL,
             `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
             `attatchment` text COLLATE utf8mb4_unicode_ci,
             `created_at` timestamp NULL DEFAULT NULL,
             `updated_at` timestamp NULL DEFAULT NULL,
             PRIMARY KEY (`id`)
           ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
            
            CREATE TABLE `invitations` (
             `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
             `team_id` int(10) UNSIGNED NOT NULL,
             `user_id` int(10) UNSIGNED DEFAULT NULL,
             `role` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
             `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
             `token` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
             `created_at` timestamp NULL DEFAULT NULL,
             `updated_at` timestamp NULL DEFAULT NULL,
             PRIMARY KEY (`id`),
             UNIQUE KEY `invitations_token_unique` (`token`)
           ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
            
            CREATE TABLE `link_list_to_columns` (
             `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
             `multiple_list_id` bigint(20) UNSIGNED NOT NULL,
             `task_list_id` bigint(20) UNSIGNED NOT NULL,
             `created_at` timestamp NULL DEFAULT NULL,
             `updated_at` timestamp NULL DEFAULT NULL,
             PRIMARY KEY (`id`)
           ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

            CREATE TABLE `multiple_boards` (
             `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
             `project_id` int(10) UNSIGNED NOT NULL,
             `nav_id` bigint(20) NOT NULL,
             `board_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
             `description` text COLLATE utf8mb4_unicode_ci,
             `created_at` timestamp NULL DEFAULT NULL,
             `updated_at` timestamp NULL DEFAULT NULL,
             PRIMARY KEY (`id`)
           ) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

            CREATE TABLE `multiple_lists` (
             `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
             `project_id` int(10) UNSIGNED NOT NULL,
             `nav_id` bigint(20) NOT NULL,
             `list_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
             `description` text COLLATE utf8mb4_unicode_ci,
             `open` tinyint(4) NOT NULL DEFAULT '1',
             `sort_id` tinyint(4) NOT NULL DEFAULT '0',
             `is_delete` tinyint(4) NOT NULL DEFAULT '0',
             `created_at` timestamp NULL DEFAULT NULL,
             `updated_at` timestamp NULL DEFAULT NULL,
             PRIMARY KEY (`id`)
           ) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
            
            CREATE TABLE `notifications` (
             `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
             `user_id` int(10) UNSIGNED NOT NULL,
             `created_by` int(10) UNSIGNED DEFAULT NULL,
             `icon` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
             `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
             `action_text` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
             `action_url` text COLLATE utf8mb4_unicode_ci,
             `read` tinyint(4) NOT NULL DEFAULT '0',
             `created_at` timestamp NULL DEFAULT NULL,
             `updated_at` timestamp NULL DEFAULT NULL,
             PRIMARY KEY (`id`)
           ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

            CREATE TABLE `notification_user` (
             `user_id` bigint(20) NOT NULL,
             `notification_id` bigint(20) NOT NULL,
             `created_at` timestamp NULL DEFAULT NULL,
             `updated_at` timestamp NULL DEFAULT NULL
           ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

            CREATE TABLE `projects` (
             `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
             `team_id` int(11) NOT NULL,
             `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
             `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
             `created_by` int(10) UNSIGNED NOT NULL,
             `updated_by` int(10) UNSIGNED NOT NULL,
             `created_at` timestamp NULL DEFAULT NULL,
             `updated_at` timestamp NULL DEFAULT NULL,
             PRIMARY KEY (`id`)
           ) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

            CREATE TABLE `project_nav_items` (
             `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
             `project_id` int(10) UNSIGNED NOT NULL,
             `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
             `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
             `sort_id` int(11) NOT NULL,
             `created_at` timestamp NULL DEFAULT NULL,
             `updated_at` timestamp NULL DEFAULT NULL,
             PRIMARY KEY (`id`)
           ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

            CREATE TABLE `project_users` (
             `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
             `project_id` int(10) UNSIGNED NOT NULL,
             `user_id` int(10) UNSIGNED NOT NULL,
             `status` tinyint(4) NOT NULL DEFAULT '0',
             `created_at` timestamp NULL DEFAULT NULL,
             `updated_at` timestamp NULL DEFAULT NULL,
             PRIMARY KEY (`id`)
           ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
            
            CREATE TABLE `rules` (
             `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
             `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
             `status` tinyint(4) NOT NULL DEFAULT '0',
             `project_id` int(10) UNSIGNED NOT NULL,
             `move_from` bigint(20) NOT NULL,
             `move_to` bigint(20) DEFAULT NULL,
             `created_by` bigint(20) NOT NULL,
             `assigned_users` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
             `created_at` timestamp NULL DEFAULT NULL,
             `updated_at` timestamp NULL DEFAULT NULL,
             PRIMARY KEY (`id`)
           ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
            
            CREATE TABLE `tags` (
             `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
             `team_id` tinyint(4) NOT NULL,
             `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
             `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
             `created_at` timestamp NULL DEFAULT NULL,
             `updated_at` timestamp NULL DEFAULT NULL,
             PRIMARY KEY (`id`)
           ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

            CREATE TABLE `task_assigned_users` (
             `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
             `user_id` int(10) UNSIGNED NOT NULL,
             `task_id` bigint(20) UNSIGNED NOT NULL,
             `created_by` int(10) UNSIGNED NOT NULL,
             `updated_by` int(10) UNSIGNED NOT NULL,
             `created_at` timestamp NULL DEFAULT NULL,
             `updated_at` timestamp NULL DEFAULT NULL,
             PRIMARY KEY (`id`)
           ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

            CREATE TABLE `task_files` (
             `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
             `file_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
             `tasks_id` bigint(20) UNSIGNED NOT NULL,
             `created_by` int(10) UNSIGNED NOT NULL,
             `updated_by` int(10) UNSIGNED NOT NULL,
             `created_at` timestamp NULL DEFAULT NULL,
             `updated_at` timestamp NULL DEFAULT NULL,
             PRIMARY KEY (`id`)
           ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
            
            CREATE TABLE `task_lists` (
             `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
             `parent_id` bigint(20) UNSIGNED DEFAULT '0',
             `project_id` int(10) UNSIGNED NOT NULL,
             `board_parent_id` bigint(20) DEFAULT NULL,
             `list_id` int(11) DEFAULT NULL,
             `multiple_board_id` int(11) DEFAULT NULL,
             `priority_label` text COLLATE utf8mb4_unicode_ci,
             `task_flag` int(11) DEFAULT NULL,
             `board_flag` int(11) DEFAULT NULL,
             `hidden` bigint(20) DEFAULT NULL,
             `sort_id` bigint(20) NOT NULL DEFAULT '0',
             `board_sort_id` bigint(20) NOT NULL DEFAULT '0',
             `created_by` int(10) UNSIGNED NOT NULL,
             `updated_by` int(10) UNSIGNED NOT NULL,
             `open` tinyint(4) NOT NULL DEFAULT '1',
             `card_open` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 = open, 1 = close',
             `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
             `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
             `color` text COLLATE utf8mb4_unicode_ci,
             `progress` text COLLATE utf8mb4_unicode_ci,
             `date` datetime NOT NULL,
             `is_complete` tinyint(4) NOT NULL DEFAULT '0',
             `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
             `copied_from` bigint(20) DEFAULT NULL,
             `deleted_at` datetime DEFAULT NULL,
             `created_at` timestamp NULL DEFAULT NULL,
             `updated_at` timestamp NULL DEFAULT NULL,
             PRIMARY KEY (`id`)
           ) ENGINE=InnoDB AUTO_INCREMENT=2992 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

            CREATE TABLE `users_details` (
             `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
             `user_id` bigint(20) UNSIGNED NOT NULL,
             `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
             `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
             `email_verified_at` timestamp NULL DEFAULT NULL,
             `photo_url` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
             `country_code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
             `phone` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
             `last_read_announcements_at` timestamp NULL DEFAULT NULL,
             `created_at` timestamp NULL DEFAULT NULL,
             `updated_at` timestamp NULL DEFAULT NULL,
             PRIMARY KEY (`id`),
             UNIQUE KEY `users_email_unique` (`email`)
           ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

            CREATE TABLE `user_filters` (
                `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
                `user_id` bigint(20) UNSIGNED NOT NULL,
                `filter_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                `created_at` timestamp NULL DEFAULT NULL,
                `updated_at` timestamp NULL DEFAULT NULL,
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";

    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `action_logs` ADD KEY `action_logs_task_id_foreign` (`task_id`);
            ALTER TABLE `assign_tags`
                ADD KEY `assign_tags_task_id_foreign` (`task_id`),
                ADD KEY `assign_tags_tag_id_foreign` (`tag_id`);
            ALTER TABLE `comments` ADD KEY `comments_task_id_foreign` (`task_id`);
            ALTER TABLE `invitations`
                ADD KEY `invitations_team_id_index` (`team_id`),
                ADD KEY `invitations_user_id_index` (`user_id`);
            ALTER TABLE `link_list_to_columns`
                ADD KEY `link_list_to_columns_multiple_list_id_foreign` (`multiple_list_id`),
                ADD KEY `link_list_to_columns_task_list_id_foreign` (`task_list_id`);
            ALTER TABLE `multiple_boards` ADD KEY `multiple_boards_project_id_foreign` (`project_id`);
            ALTER TABLE `multiple_lists` ADD KEY `multiple_lists_project_id_foreign` (`project_id`);
            ALTER TABLE `notifications` ADD KEY `notifications_user_id_created_at_index` (`user_id`,`created_at`);
            ALTER TABLE `project_nav_items` ADD KEY `project_nav_items_project_id_foreign` (`project_id`);
            ALTER TABLE `project_users`
                ADD KEY `project_users_project_id_index` (`project_id`),
                ADD KEY `project_users_user_id_index` (`user_id`);
            ALTER TABLE `rules` ADD KEY `rules_project_id_foreign` (`project_id`);
            ALTER TABLE `task_assigned_users` ADD KEY `task_assigned_users_task_id_foreign` (`task_id`);
            ALTER TABLE `task_files` ADD KEY `task_files_tasks_id_foreign` (`tasks_id`);
            ALTER TABLE `task_lists` ADD KEY `task_lists_project_id_foreign` (`project_id`);
            ALTER TABLE `users_details` ADD KEY `users_details_user_id_index` (`user_id`);
            ALTER TABLE `user_filters` ADD KEY `user_filters_user_id_foreign` (`user_id`);";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `action_logs` ADD CONSTRAINT `action_logs_task_id_foreign` FOREIGN KEY (`task_id`) REFERENCES `task_lists` (`id`) ON DELETE CASCADE;";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `assign_tags`
             ADD CONSTRAINT `assign_tags_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE,
             ADD CONSTRAINT `assign_tags_task_id_foreign` FOREIGN KEY (`task_id`) REFERENCES `task_lists` (`id`) ON DELETE CASCADE;";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `comments` ADD CONSTRAINT `comments_task_id_foreign` FOREIGN KEY (`task_id`) REFERENCES `task_lists` (`id`) ON DELETE CASCADE;";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `link_list_to_columns`
             ADD CONSTRAINT `link_list_to_columns_multiple_list_id_foreign` FOREIGN KEY (`multiple_list_id`) REFERENCES `multiple_lists` (`id`) ON DELETE CASCADE,
             ADD CONSTRAINT `link_list_to_columns_task_list_id_foreign` FOREIGN KEY (`task_list_id`) REFERENCES `task_lists` (`id`) ON DELETE CASCADE;";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `multiple_boards` ADD CONSTRAINT `multiple_boards_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE;";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `multiple_lists` ADD CONSTRAINT `multiple_lists_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE;";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `project_nav_items` ADD CONSTRAINT `project_nav_items_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE;";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `project_users` ADD CONSTRAINT `project_users_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE;";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `rules` ADD CONSTRAINT `rules_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE;";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `task_assigned_users` ADD CONSTRAINT `task_assigned_users_task_id_foreign` FOREIGN KEY (`task_id`) REFERENCES `task_lists` (`id`) ON DELETE CASCADE;";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `task_files` ADD CONSTRAINT `task_files_tasks_id_foreign` FOREIGN KEY (`tasks_id`) REFERENCES `task_lists` (`id`) ON DELETE CASCADE;";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `task_lists` ADD CONSTRAINT `task_lists_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE;";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `user_filters` ADD CONSTRAINT `user_filters_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users_details` (`user_id`) ON DELETE CASCADE;";
    $team_db_connect->exec($sql);
}