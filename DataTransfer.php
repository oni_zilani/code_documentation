<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$old_db_name = 'sparktask_old';
$new_base_db = 'sparktask_new';
$team_db_name = 'compltit_db_team';//compltit_db_team
$servername = 'localhost';
$username = 'root';
$password = '';

try {
    $conn = new PDO("mysql:host=$servername;dbname=$old_db_name", $username, $password);
    $conn_new = new PDO("mysql:host=$servername;dbname=$new_base_db", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $conn_new->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $sql_users = "SELECT * FROM users";
    $result = $conn->query($sql_users);
    if ($result->rowCount() > 0) {
        while ($user = $result->fetch(PDO::FETCH_OBJ)) {
            $sql = "INSERT INTO users (id,name,email,email_verified_at,password,remember_token,photo_url,uses_two_factor_auth,authy_id,country_code,
                    phone,two_factor_reset_code,current_team_id,stripe_id,current_billing_plan,card_brand, card_last_four,card_country,billing_address,billing_address_line_2,billing_city,billing_state,billing_zip,billing_country,vat_id,
                    extra_billing_information,trial_ends_at,last_read_announcements_at)
                    VALUES ('$user->id','$user->name','$user->email','$user->email_verified_at','$user->password','$user->remember_token','$user->photo_url','$user->uses_two_factor_auth',
                    '$user->authy_id','$user->country_code','$user->phone','$user->two_factor_reset_code','$user->current_team_id','$user->stripe_id','$user->current_billing_plan','$user->card_brand','
                    $user->card_last_four','$user->card_country','$user->billing_address','$user->billing_address_line_2','$user->billing_city','$user->billing_state','$user->billing_zip','$user->billing_country','
                    $user->vat_id','$user->extra_billing_information','$user->trial_ends_at','$user->last_read_announcements_at')";
            $conn_new->query($sql);
        }
    }
    $sql_team_user = "SELECT * FROM team_users";
    $team_users = $conn->query($sql_team_user);
    if ($team_users->rowCount() > 0) {
        while ($team_user = $team_users->fetch(PDO::FETCH_OBJ)) {
            $sql = "INSERT INTO team_users (team_id,user_id,role)
                    VALUES ('$team_user->team_id','$team_user->user_id','$team_user->role')";
            $conn_new->query($sql);
        }
    }


    $sql_teams = "SELECT * FROM teams";
    $result_team = $conn->query($sql_teams);
    if ($result_team->rowCount() > 0) {
        while ($team = $result_team->fetch(PDO::FETCH_OBJ)) {
            $sql = "INSERT INTO teams (id, owner_id, name)
                    VALUES ('$team->id','$team->owner_id','$team->name')";
            $conn_new->query($sql);
            try {
                $dbh = new PDO("mysql:host=$servername", $username, $password);
                $team_db = $team_db_name . $team->id;

                //create database
                $dbh->exec("CREATE DATABASE `$team_db`;
                CREATE USER '$username'@'localhost' IDENTIFIED BY '';
                GRANT ALL ON `$team_db`.* TO '$username'@'localhost';
                FLUSH PRIVILEGES;");
                //create tables
                CreateTeamDB($team_db, $servername, $username, $password);

                $team_db_connect = new PDO("mysql:host=$servername;dbname=$team_db", $username, $password);
                $sql = "INSERT INTO `users_details` ( `user_id`) VALUES ('$team->owner_id')";
                $team_db_connect->query($sql);

                $sql_tags = "SELECT * FROM tags where team_id = " . $team->id;
                $result_tags = $conn->query($sql_tags);
                if ($result_tags->rowCount() > 0) {
                    while ($tagData = $result_tags->fetch(PDO::FETCH_OBJ)) {

                        $sql = "INSERT INTO tags (`id`, `team_id`, `title`, `color`, `created_at`, `updated_at`)
                                VALUES ('$tagData->id', '$tagData->team_id', '$tagData->title', '$tagData->color', '$tagData->created_at', '$tagData->updated_at')";
                        $team_db_connect->query($sql);
                    }
                }


                $sql_projects = "SELECT * FROM projects where team_id = " . $team->id;
                $result_project = $conn->query($sql_projects);

                if ($result_project->rowCount() > 0) {
                    while ($project = $result_project->fetch(PDO::FETCH_OBJ)) {
                        $sql = "INSERT INTO projects (id, team_id, name, description, created_by, updated_by, created_at, updated_at)
                                VALUES ('$project->id',$project->team_id,'$project->name','$project->description',$project->created_by,'$project->updated_by','$project->created_at','$project->updated_at')";
                        $team_db_connect->query($sql);


                        $sql_project_rules = "SELECT * FROM rules where project_id = " . $project->id;
                        $result_project_rules = $conn->query($sql_project_rules);
                        if ($result_project_rules->rowCount() > 0) {
                            while ($rules_data = $result_project_rules->fetch(PDO::FETCH_OBJ)) {
                                $sql = "INSERT INTO rules (`id`, `name`, `status`, `project_id`, `move_from`, `move_to`, `created_by`, `assigned_users`, `created_at`, `updated_at`) 
                                    VALUES ('$rules_data->id','$rules_data->name','$rules_data->status','$rules_data->project_id','$rules_data->move_from','$rules_data->move_to','$rules_data->created_by','$rules_data->assigned_users','$rules_data->created_at','$rules_data->updated_at')";
                                $team_db_connect->query($sql);
                            }
                        }

                        $sql_project_navs = "SELECT * FROM project_nav_items where project_id = " . $project->id;
                        $result_nav = $conn->query($sql_project_navs);
                        if ($result_nav->rowCount() > 0) {
                            while ($nav = $result_nav->fetch(PDO::FETCH_OBJ)) {

                                $sql = "INSERT INTO project_nav_items (id,project_id,title,type,sort_id,created_at,updated_at)
                                            VALUES ('$nav->id','$nav->project_id', '$nav->title','$nav->type', '$nav->sort_id','$nav->created_at','$nav->updated_at')";
                                $team_db_connect->query($sql);
                            }
                        }

                        $sql_project_board = "SELECT * FROM multiple_boards where project_id = " . $project->id;
                        $result_board = $conn->query($sql_project_board);

                        if ($result_board->rowCount() > 0) {
                            while ($board = $result_board->fetch(PDO::FETCH_OBJ)) {
                                $sql = "INSERT INTO multiple_boards (id,project_id,nav_id,board_title,description,created_at,updated_at)
                                            VALUES ('$board->id','$board->project_id','$board->nav_id','$board->board_title','$board->board_title','$board->created_at','$board->updated_at')";
                                $team_db_connect->query($sql);
                            }
                        }

                        $sql_project_Lists = "SELECT * FROM multiple_lists where project_id = " . $project->id;
                        $result_list = $conn->query($sql_project_Lists);

                        if ($result_list->rowCount() > 0) {
                            while ($list = $result_list->fetch(PDO::FETCH_OBJ)) {
                                $sql = "INSERT INTO  multiple_lists (id,project_id,sort_id,nav_id,list_title,description,open,is_delete,created_at,updated_at)
                                            VALUES ('$list->id' ,'$list->project_id','$list->sort_id','$list->nav_id','$list->list_title','$list->description','$list->open','$list->is_delete','$list->created_at','$list->updated_at')";
                                $team_db_connect->query($sql);
                            }
                        }


                        $sql_project_tasks = "SELECT * FROM task_lists where project_id = " . $project->id;
                        $result_tasks = $conn->query($sql_project_tasks);

                        if ($result_tasks->rowCount() > 0) {
                            while ($task = $result_tasks->fetch(PDO::FETCH_OBJ)) {
                                $parent_id = ($task->parent_id !== NULL) ? $task->parent_id : "NULL";
                                $board_parent_id = ($task->board_parent_id !== NULL) ? $task->board_parent_id : "NULL";
                                $list_id = ($task->list_id !== NULL) ? $task->list_id : "NULL";
                                $multiple_board_id = ($task->multiple_board_id !== NULL) ? $task->multiple_board_id : "NULL";
                                $priority_label = ($task->priority_label !== NULL) ? $task->priority_label : "NULL";
                                $task_flag = ($task->task_flag !== NULL) ? $task->task_flag : "NULL";
                                $board_flag = ($task->board_flag !== NULL) ? $task->board_flag : "NULL";
                                $hidden = ($task->hidden !== NULL) ? $task->hidden : "NULL";
                                $color = ($task->color !== NULL) ? $task->color : "NULL";
                                $progress = ($task->progress !== NULL) ? $task->progress : "NULL";
                                $deleted_at = ($task->deleted_at !== NULL) ? $task->deleted_at : "NULL";
                                $copied_from = ($task->copied_from !== NULL) ? $task->copied_from : "NULL";

                                $sql = "INSERT INTO task_lists (id,parent_id,project_id,board_parent_id,list_id,multiple_board_id,priority_label,task_flag,board_flag,hidden,sort_id,board_sort_id,created_by,updated_by,open,card_open,title,description,color,progress,date,is_complete,is_deleted,deleted_at,created_at,updated_at,copied_from)
                                            VALUES ($task->id,$parent_id,$task->project_id,$board_parent_id,$list_id,$multiple_board_id,$priority_label,$task_flag,$board_flag,$hidden,$task->sort_id,$task->board_sort_id,$task->created_by,$task->updated_by,$task->open,$task->card_open,'$task->title',
                                            '$task->description','$color',$progress ,'$task->date',$task->is_complete,$task->is_deleted,$deleted_at,'$task->created_at','$task->updated_at',$copied_from)";

                                $team_db_connect->query($sql);

                                $sql_task_assign = "SELECT * FROM task_assigned_users where task_id = " . $task->id;
                                $result_task_assign = $conn->query($sql_task_assign);
                                if ($result_task_assign->rowCount() > 0) {
                                    while ($task_assigned_users_data = $result_task_assign->fetch(PDO::FETCH_OBJ)) {
                                        $sql = "INSERT INTO task_assigned_users (`id`, `user_id`, `task_id`, `created_by`, `updated_by`, `created_at`, `updated_at`) 
                                                VALUES ('$task_assigned_users_data->id','$task_assigned_users_data->user_id','$task_assigned_users_data->task_id','$task_assigned_users_data->created_by','$task_assigned_users_data->updated_by','$task_assigned_users_data->created_at','$task_assigned_users_data->updated_at')";
                                        $team_db_connect->query($sql);
                                    }
                                }
                                //task file
                                $sql_task_file = "SELECT * FROM task_files where tasks_id = " . $task->id;
                                $result_task_file = $conn->query($sql_task_file);
                                if ($result_task_file->rowCount() > 0) {
                                    while ($task_file_data = $result_task_file->fetch(PDO::FETCH_OBJ)) {

                                        $sql_task_check = "SELECT * FROM task_lists where id = " . $task_file_data->tasks_id;
                                        $result_task_check = $team_db_connect->query($sql_task_check);
                                        if ($result_task_check->rowCount() > 0) {
                                            $sql = "INSERT INTO task_files (`id`, `file_name`, `tasks_id`, `created_by`, `updated_by`, `created_at`, `updated_at`) 
                                                        VALUES ('$task_file_data->id','$task_file_data->file_name','$task_file_data->tasks_id','$task_file_data->created_by','$task_file_data->updated_by','$task_file_data->created_at','$task_file_data->updated_at')";
                                            $team_db_connect->query($sql);
                                        }
                                    }
                                }
                                //task comments
                                $sql_task_comments = "SELECT * FROM comments where task_id = " . $task->id;
                                $result_task_comments = $conn->query($sql_task_comments);
                                if ($result_task_comments->rowCount() > 0) {
                                    while ($comments_data = $result_task_comments->fetch(PDO::FETCH_OBJ)) {

                                        $sql_task_check = "SELECT * FROM task_lists where id = " . $comments_data->task_id;
                                        $result_task_check = $team_db_connect->query($sql_task_check);
                                        if ($result_task_check->rowCount() > 0) {
                                            $parent_id = ($comments_data->parent_id !== NULL) ? $comments_data->parent_id : "NULL";
                                            $sql = $sql = "INSERT INTO comments (`id`, `task_id`, `user_id`, `parent_id`, `comment`, `attatchment`, `created_at`, `updated_at`) 
                                                                VALUES ('$comments_data->id','$comments_data->task_id','$comments_data->user_id','$parent_id','$comments_data->comment','$comments_data->attatchment','$comments_data->created_at','$comments_data->updated_at')";
                                            $team_db_connect->query($sql);
                                        }
                                    }
                                }
                                //task tag assign
                                $sql_task_assign_tag = "SELECT * FROM assign_tags where task_id = " . $task->id;
                                $result_task_assign_tag = $conn->query($sql_task_assign_tag);
                                if ($result_task_assign_tag->rowCount() > 0) {
                                    while ($assign_tags_data = $result_task_assign_tag->fetch(PDO::FETCH_OBJ)) {

                                        $sql_task_check = "SELECT * FROM task_lists where id = " . $assign_tags_data->task_id;
                                        $result_task_check = $team_db_connect->query($sql_task_check);
                                        $sql_tags_check = "SELECT * FROM tags where id = " . $assign_tags_data->tag_id;
                                        $result_tags_check = $team_db_connect->query($sql_tags_check);

                                        if ($result_task_check->rowCount() > 0 && $result_tags_check->rowCount() > 0) {
                                            $sql = "INSERT INTO assign_tags (`id`, `task_id`, `tag_id`, `created_at`, `updated_at`) 
                                                        VALUES ('$assign_tags_data->id','$assign_tags_data->task_id','$assign_tags_data->tag_id','$assign_tags_data->created_at','$assign_tags_data->updated_at')";
                                            $team_db_connect->query($sql);
                                        }
                                    }
                                }
                            }
                        }


                    }
                }
            } catch (PDOException $e) {
                echo "Connection failed: " . $e->getMessage();
            }
        }
        echo "Data Copy Successful";
    }


} catch (PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
}


function CreateTeamDB($db_name, $servername, $username, $password)
{
    $team_db_connect = new PDO("mysql:host=$servername;dbname=$db_name", $username, $password);
    $sql = "CREATE TABLE `action_logs` (
                        `id` bigint(20) UNSIGNED NOT NULL,
                        `project_id` int(11) DEFAULT NULL,
                        `multiple_list_id` bigint(20) DEFAULT NULL,
                        `task_id` bigint(20) UNSIGNED DEFAULT NULL,
                        `multiple_board_id` bigint(20) DEFAULT NULL,
                        `board_id` bigint(20) DEFAULT NULL,
                        `title` text COLLATE utf8mb4_unicode_ci,
                        `log_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                        `action_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                        `action_by` int(11) NOT NULL,
                        `action_at` datetime NOT NULL
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
    $team_db_connect->exec($sql);
    $sql = "CREATE TABLE `assign_tags` (
              `id` bigint(20) UNSIGNED NOT NULL,
              `task_id` bigint(20) UNSIGNED NOT NULL,
              `tag_id` bigint(20) UNSIGNED NOT NULL,
              `created_at` timestamp NULL DEFAULT NULL,
              `updated_at` timestamp NULL DEFAULT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci";
    $team_db_connect->exec($sql);
    $sql = "CREATE TABLE `comments` (
              `id` bigint(20) UNSIGNED NOT NULL,
              `task_id` bigint(20) UNSIGNED NOT NULL,
              `user_id` bigint(20) NOT NULL,
              `parent_id` bigint(20) DEFAULT NULL,
              `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
              `attatchment` text COLLATE utf8mb4_unicode_ci,
              `created_at` timestamp NULL DEFAULT NULL,
              `updated_at` timestamp NULL DEFAULT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
    $team_db_connect->exec($sql);
    $sql = "CREATE TABLE `invitations` (
              `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
              `team_id` int(10) UNSIGNED NOT NULL,
              `user_id` int(10) UNSIGNED DEFAULT NULL,
              `role` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
              `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
              `token` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
              `created_at` timestamp NULL DEFAULT NULL,
              `updated_at` timestamp NULL DEFAULT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
    $team_db_connect->exec($sql);
    $sql = "CREATE TABLE `link_list_to_columns` (
              `id` bigint(20) UNSIGNED NOT NULL,
              `multiple_list_id` bigint(20) UNSIGNED NOT NULL,
              `task_list_id` bigint(20) UNSIGNED NOT NULL,
              `created_at` timestamp NULL DEFAULT NULL,
              `updated_at` timestamp NULL DEFAULT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
    $team_db_connect->exec($sql);
    $sql = "CREATE TABLE `multiple_boards` (
              `id` bigint(20) UNSIGNED NOT NULL,
              `project_id` int(10) UNSIGNED NOT NULL,
              `nav_id` bigint(20) NOT NULL,
              `board_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
              `description` text COLLATE utf8mb4_unicode_ci,
              `created_at` timestamp NULL DEFAULT NULL,
              `updated_at` timestamp NULL DEFAULT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
    $team_db_connect->exec($sql);
    $sql = "CREATE TABLE `multiple_lists` (
              `id` bigint(20) UNSIGNED NOT NULL,
              `project_id` int(10) UNSIGNED NOT NULL,
              `nav_id` bigint(20) NOT NULL,
              `list_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
              `description` text COLLATE utf8mb4_unicode_ci,
              `open` tinyint(4) NOT NULL DEFAULT '1',
              `sort_id` tinyint(4) NOT NULL DEFAULT '0',
              `is_delete` tinyint(4) NOT NULL DEFAULT '0',
              `created_at` timestamp NULL DEFAULT NULL,
              `updated_at` timestamp NULL DEFAULT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
    $team_db_connect->exec($sql);
    $sql = "CREATE TABLE `notifications` (
              `id` int(10) UNSIGNED NOT NULL,
              `user_id` int(10) UNSIGNED NOT NULL,
              `created_by` int(10) UNSIGNED DEFAULT NULL,
              `icon` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
              `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
              `action_text` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
              `action_url` text COLLATE utf8mb4_unicode_ci,
              `read` tinyint(4) NOT NULL DEFAULT '0',
              `created_at` timestamp NULL DEFAULT NULL,
              `updated_at` timestamp NULL DEFAULT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
    $team_db_connect->exec($sql);
    $sql = "CREATE TABLE `notification_user` (
              `user_id` bigint(20) NOT NULL,
              `notification_id` bigint(20) NOT NULL,
              `created_at` timestamp NULL DEFAULT NULL,
              `updated_at` timestamp NULL DEFAULT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
    $team_db_connect->exec($sql);
    $sql = "CREATE TABLE `performance_indicators` (
              `id` int(10) UNSIGNED NOT NULL,
              `monthly_recurring_revenue` decimal(8,2) NOT NULL,
              `yearly_recurring_revenue` decimal(8,2) NOT NULL,
              `daily_volume` decimal(8,2) NOT NULL,
              `new_users` int(11) NOT NULL,
              `created_at` timestamp NULL DEFAULT NULL,
              `updated_at` timestamp NULL DEFAULT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
    $team_db_connect->exec($sql);
    $sql = "CREATE TABLE `projects` (
              `id` int(10) UNSIGNED NOT NULL,
              `team_id` int(11) NOT NULL,
              `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
              `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
              `created_by` int(10) UNSIGNED NOT NULL,
              `updated_by` int(10) UNSIGNED NOT NULL,
              `created_at` timestamp NULL DEFAULT NULL,
              `updated_at` timestamp NULL DEFAULT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
    $team_db_connect->exec($sql);
    $sql = "CREATE TABLE `project_nav_items` (
              `id` bigint(20) UNSIGNED NOT NULL,
              `project_id` int(10) UNSIGNED NOT NULL,
              `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
              `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
              `sort_id` int(11) NOT NULL,
              `created_at` timestamp NULL DEFAULT NULL,
              `updated_at` timestamp NULL DEFAULT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
    $team_db_connect->exec($sql);
    $sql = "CREATE TABLE `project_users` (
              `id` bigint(20) UNSIGNED NOT NULL,
              `project_id` int(10) UNSIGNED NOT NULL,
              `user_id` int(10) UNSIGNED NOT NULL,
              `status` tinyint(4) NOT NULL DEFAULT '0',
              `created_at` timestamp NULL DEFAULT NULL,
              `updated_at` timestamp NULL DEFAULT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
    $team_db_connect->exec($sql);
    $sql = "CREATE TABLE `rules` (
              `id` bigint(20) UNSIGNED NOT NULL,
              `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
              `status` tinyint(4) NOT NULL DEFAULT '0',
              `project_id` int(10) UNSIGNED NOT NULL,
              `move_from` bigint(20) NOT NULL,
              `move_to` bigint(20) DEFAULT NULL,
              `created_by` bigint(20) NOT NULL,
              `assigned_users` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
              `created_at` timestamp NULL DEFAULT NULL,
              `updated_at` timestamp NULL DEFAULT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
    $team_db_connect->exec($sql);
    $sql = "CREATE TABLE `tags` (
              `id` bigint(20) UNSIGNED NOT NULL,
              `team_id` tinyint(4) NOT NULL,
              `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
              `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
              `created_at` timestamp NULL DEFAULT NULL,
              `updated_at` timestamp NULL DEFAULT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
    $team_db_connect->exec($sql);
    $sql = "CREATE TABLE `task_assigned_users` (
              `id` bigint(20) UNSIGNED NOT NULL,
              `user_id` int(10) UNSIGNED NOT NULL,
              `task_id` bigint(20) UNSIGNED NOT NULL,
              `created_by` int(10) UNSIGNED NOT NULL,
              `updated_by` int(10) UNSIGNED NOT NULL,
              `created_at` timestamp NULL DEFAULT NULL,
              `updated_at` timestamp NULL DEFAULT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
    $team_db_connect->exec($sql);
    $sql = "CREATE TABLE `task_files` (
              `id` bigint(20) UNSIGNED NOT NULL,
              `file_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
              `tasks_id` bigint(20) UNSIGNED NOT NULL,
              `created_by` int(10) UNSIGNED NOT NULL,
              `updated_by` int(10) UNSIGNED NOT NULL,
              `created_at` timestamp NULL DEFAULT NULL,
              `updated_at` timestamp NULL DEFAULT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
    $team_db_connect->exec($sql);
    $sql = "CREATE TABLE `task_lists` (
              `id` bigint(20) UNSIGNED NOT NULL,
              `parent_id` bigint(20) UNSIGNED DEFAULT '0',
              `project_id` int(10) UNSIGNED NOT NULL,
              `board_parent_id` bigint(20) DEFAULT NULL,
              `list_id` int(11) DEFAULT NULL,
              `multiple_board_id` int(11) DEFAULT NULL,
              `priority_label` text COLLATE utf8mb4_unicode_ci,
              `task_flag` int(11) DEFAULT NULL,
              `board_flag` int(11) DEFAULT NULL,
              `hidden` bigint(20) DEFAULT NULL,
              `sort_id` bigint(20) NOT NULL DEFAULT '0',
              `board_sort_id` bigint(20) NOT NULL DEFAULT '0',
              `created_by` int(10) UNSIGNED NOT NULL,
              `updated_by` int(10) UNSIGNED NOT NULL,
              `open` tinyint(4) NOT NULL DEFAULT '1',
              `card_open` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 = open, 1 = close',
              `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
              `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
              `color` text COLLATE utf8mb4_unicode_ci,
              `progress` text COLLATE utf8mb4_unicode_ci,
              `date` datetime NOT NULL,
              `is_complete` tinyint(4) NOT NULL DEFAULT '0',
              `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
              `copied_from` bigint(20) DEFAULT NULL,
              `deleted_at` datetime DEFAULT NULL,
              `created_at` timestamp NULL DEFAULT NULL,
              `updated_at` timestamp NULL DEFAULT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
    $team_db_connect->exec($sql);
    $sql = "CREATE TABLE `users_details` (
              `id` bigint(20) UNSIGNED NOT NULL,
              `user_id` bigint(20) UNSIGNED NOT NULL,
              `country_code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
              `phone` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
              `two_factor_reset_code` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
              `current_team_id` int(11) DEFAULT NULL,
              `stripe_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
              `current_billing_plan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
              `card_brand` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
              `card_last_four` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
              `card_country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
              `billing_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
              `billing_address_line_2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
              `billing_city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
              `billing_state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
              `billing_zip` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
              `billing_country` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
              `vat_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
              `extra_billing_information` text COLLATE utf8mb4_unicode_ci,
              `trial_ends_at` timestamp NULL DEFAULT NULL,
              `last_read_announcements_at` timestamp NULL DEFAULT NULL,
              `created_at` timestamp NULL DEFAULT NULL,
              `updated_at` timestamp NULL DEFAULT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
    $team_db_connect->exec($sql);
    $sql = "CREATE TABLE `user_filters` (
          `id` bigint(20) UNSIGNED NOT NULL,
          `user_id` bigint(20) UNSIGNED NOT NULL,
          `filter_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
          `created_at` timestamp NULL DEFAULT NULL,
          `updated_at` timestamp NULL DEFAULT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `action_logs`ADD PRIMARY KEY (`id`),ADD KEY `action_logs_task_id_foreign` (`task_id`);";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `assign_tags`
              ADD PRIMARY KEY (`id`),
              ADD KEY `assign_tags_task_id_foreign` (`task_id`),
              ADD KEY `assign_tags_tag_id_foreign` (`tag_id`);";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `comments`
              ADD PRIMARY KEY (`id`),
              ADD KEY `comments_task_id_foreign` (`task_id`);";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `invitations`
              ADD PRIMARY KEY (`id`),
              ADD UNIQUE KEY `invitations_token_unique` (`token`),
              ADD KEY `invitations_team_id_index` (`team_id`),
              ADD KEY `invitations_user_id_index` (`user_id`);";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `link_list_to_columns`
              ADD PRIMARY KEY (`id`),
              ADD KEY `link_list_to_columns_multiple_list_id_foreign` (`multiple_list_id`),
              ADD KEY `link_list_to_columns_task_list_id_foreign` (`task_list_id`);";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `multiple_boards`
              ADD PRIMARY KEY (`id`),
              ADD KEY `multiple_boards_project_id_foreign` (`project_id`);";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `multiple_lists`
              ADD PRIMARY KEY (`id`),
              ADD KEY `multiple_lists_project_id_foreign` (`project_id`);";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `notifications`
              ADD PRIMARY KEY (`id`),
              ADD KEY `notifications_user_id_created_at_index` (`user_id`,`created_at`);";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `performance_indicators`
              ADD PRIMARY KEY (`id`),
              ADD KEY `performance_indicators_created_at_index` (`created_at`);";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `projects`
                ADD PRIMARY KEY (`id`);";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `project_nav_items`
              ADD PRIMARY KEY (`id`),
              ADD KEY `project_nav_items_project_id_foreign` (`project_id`);";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `project_users`
              ADD PRIMARY KEY (`id`),
              ADD KEY `project_users_project_id_index` (`project_id`),
              ADD KEY `project_users_user_id_index` (`user_id`);";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `rules`
              ADD PRIMARY KEY (`id`),
              ADD KEY `rules_project_id_foreign` (`project_id`);";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `tags`
                ADD PRIMARY KEY (`id`);";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `task_assigned_users`
              ADD PRIMARY KEY (`id`),
              ADD KEY `task_assigned_users_task_id_foreign` (`task_id`);";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `task_files`
              ADD PRIMARY KEY (`id`),
              ADD KEY `task_files_tasks_id_foreign` (`tasks_id`);";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `task_lists`
              ADD PRIMARY KEY (`id`),
              ADD KEY `task_lists_project_id_foreign` (`project_id`);";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `users_details`
              ADD PRIMARY KEY (`id`),
              ADD KEY `users_details_user_id_index` (`user_id`);";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `user_filters`
              ADD PRIMARY KEY (`id`),
              ADD KEY `user_filters_user_id_foreign` (`user_id`);";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `action_logs`
             MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `assign_tags`
                MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `comments`
                MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `link_list_to_columns`
                MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `multiple_boards`
                MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123;";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `multiple_lists`
                MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=147;";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `notifications`
                MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `performance_indicators`
                MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `projects`
                MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `project_nav_items`
                MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=140;";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `project_users`
                MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `rules`
                MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `tags`
                MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `task_assigned_users`
                MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `task_files`
                MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `task_lists`
                MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1703;";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `users_details`
                MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `user_filters`
                MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `action_logs`
                ADD CONSTRAINT `action_logs_task_id_foreign` FOREIGN KEY (`task_id`) REFERENCES `task_lists` (`id`) ON DELETE CASCADE;";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `assign_tags`
              ADD CONSTRAINT `assign_tags_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE,
              ADD CONSTRAINT `assign_tags_task_id_foreign` FOREIGN KEY (`task_id`) REFERENCES `task_lists` (`id`) ON DELETE CASCADE;";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `comments`
                ADD CONSTRAINT `comments_task_id_foreign` FOREIGN KEY (`task_id`) REFERENCES `task_lists` (`id`) ON DELETE CASCADE;";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `link_list_to_columns`
              ADD CONSTRAINT `link_list_to_columns_multiple_list_id_foreign` FOREIGN KEY (`multiple_list_id`) REFERENCES `multiple_lists` (`id`) ON DELETE CASCADE,
              ADD CONSTRAINT `link_list_to_columns_task_list_id_foreign` FOREIGN KEY (`task_list_id`) REFERENCES `task_lists` (`id`) ON DELETE CASCADE;";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `multiple_boards`
                ADD CONSTRAINT `multiple_boards_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE;";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `multiple_lists`
                ADD CONSTRAINT `multiple_lists_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE;";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `project_nav_items`
                ADD CONSTRAINT `project_nav_items_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE;";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `project_users`
                ADD CONSTRAINT `project_users_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE;";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `rules`
                ADD CONSTRAINT `rules_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE;";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `task_assigned_users`
                ADD CONSTRAINT `task_assigned_users_task_id_foreign` FOREIGN KEY (`task_id`) REFERENCES `task_lists` (`id`) ON DELETE CASCADE;";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `task_files`
                ADD CONSTRAINT `task_files_tasks_id_foreign` FOREIGN KEY (`tasks_id`) REFERENCES `task_lists` (`id`) ON DELETE CASCADE;";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `task_lists`
                ADD CONSTRAINT `task_lists_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE;";
    $team_db_connect->exec($sql);
    $sql = "ALTER TABLE `user_filters`
                ADD CONSTRAINT `user_filters_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users_details` (`user_id`) ON DELETE CASCADE;";
    $team_db_connect->exec($sql);

}